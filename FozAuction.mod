<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" >

    <UiMod name="FozAuction" version="1.3" date="11/17/2010" >
        <Author name="Fozee" email="skuzfoz@gmail.com" />
        <Description text="Proof of concept modification of the default EA AuctionHouse window." />
		<Replaces name="EA_AuctionHouseWindow" />
        <Dependencies>
            <Dependency name="EASystem_Utils" />
            <Dependency name="EASystem_WindowUtils" />
            <Dependency name="EATemplate_DefaultWindowSkin" />
            <Dependency name="EASystem_Tooltips" />
            <Dependency name="EASystem_ResourceFrames" />
            <Dependency name="EA_Cursor" />
        </Dependencies>
        <Files>
            <File name="Source/Templates_AuctionWindow.xml" />
            <File name="Source/AuctionWindowSearchControls.xml" />
            <File name="Source/AuctionWindowSellControls.xml" />
            <File name="Source/AuctionWindow.xml" />
        </Files>
        <OnInitialize>
            <CreateWindow name="AuctionWindow" show="false" />
        </OnInitialize>
    </UiMod>

</ModuleFile>
